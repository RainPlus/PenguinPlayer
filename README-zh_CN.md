[English](README.md) | 中文（简体）

# 🐧 Penguin Player - 重生
一个为嵌入而设计的模块化播放器。

## 🚧 施工中
在找可以使用的**旧**版本？

查看 [Releases](https://github.com/RainPlus-Team/PenguinPlayer/releases) 或者 [nonsem-1.2.x](https://github.com/RainPlus-Team/PenguinPlayer/tree/nonsem-1.2.x) 分支。