# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- A new demo page
- Well-maintained player code
- Modularized player
- Optional and themeable user interface
- Support [QQ Music](https://y.qq.com)
- Support [KuGou Music](https://kugou.com)

### Removed
- __Everything__