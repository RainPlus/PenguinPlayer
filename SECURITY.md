# Security Policy

## Supported Versions

Following table shows that if a version is still getting updates or not.

:white_check_mark: - Supported

:o: - Only bug fixes

:x: - Not supported

| Version         | Supported          |
|-----------------|--------------------|
| 0.x             | :white_check_mark: |
| NonSem versions | :x:                |

## Reporting a Vulnerability

If you found a vulnerability in this project, contact us at [luorain@m4tec.org](mailto:luorain@m4tec.org) or submit a new issue.

Describe the vulnerability in detail, we will review it as soon as possible. If necessary, we will release a new version to fix the vulnerability for unsupported versions too.
